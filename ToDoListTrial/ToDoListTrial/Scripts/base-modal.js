﻿var BaseModal = function () {
    var $modal = $("#general-modal");
    
       $("#general-modal").modal({
                show: false,
                backdrop: 'static'

            });
      

    return {

        initComponents: function () {
            $modal.modal();
            ComponentsDateTimePickers.init();
            ComponentsSelect2.init();
            FormInputMask.init();
        }

    }

}();


jQuery(document).ready(function () {
    BaseModal.initComponents();
});



