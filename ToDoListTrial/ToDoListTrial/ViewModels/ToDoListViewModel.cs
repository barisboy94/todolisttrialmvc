﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToDoListTrial.Models;

namespace ToDoListTrial.ViewModels
{
    public class ToDoListViewModel
    {
        public int ToDoListId { get; set; }
        public string ToDoListTitle { get; set; }

        public IEnumerable<ToDoList> ToDoLists { get; set; }
        public IEnumerable<ToDoItem> ToDoItems { get; set; }

        public string Message { get; set; }
    }
}