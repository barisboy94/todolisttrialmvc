﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ToDoListTrial.Context
{
    public class ToDoInitializer : CreateDatabaseIfNotExists<ToDoContext>
    {
        protected override void Seed(ToDoContext context)
        {
            base.Seed(context);
        }
    }
}