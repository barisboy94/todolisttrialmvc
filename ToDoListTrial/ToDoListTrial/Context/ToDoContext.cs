﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using ToDoListTrial.Models;

namespace ToDoListTrial.Context
{
    public class ToDoContext : DbContext
    {
        public ToDoContext() : base ("ToDoContext")
        {

        }

        public DbSet<ToDoList> ToDoLists { get; set; }

        public DbSet<ToDoItem> ToDoItems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}