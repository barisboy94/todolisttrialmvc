﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ToDoListTrial.Context;
using ToDoListTrial.Models;
using ToDoListTrial.ViewModels;

namespace ToDoListTrial.Controllers
{
    public class ToDoListController : Controller
    {
        private ToDoContext db = new ToDoContext();

        // GET: ToDoList
        public ActionResult Index()
        {

            var model = new ToDoListViewModel();
            var toDoLists = db.ToDoLists.ToList();

            foreach (var toDoList in toDoLists)
            {
                toDoList.ToDoItems = GetTodoItems(toDoList.Id);
            }
            model.ToDoLists = toDoLists.OrderByDescending(r => r.CreateDate);

            return View(model);
        }

        public IEnumerable<ToDoItem> GetTodoItems(int toDoListId)
        {
            var listItems = db.ToDoItems.ToList().Where(r => r.ToDoListId == toDoListId);

            return listItems;
        }

        [HttpPost]
        public JsonResult GetExpiredItems()
        {
            var toDoLists = db.ToDoLists.ToList();

            foreach (var toDoList in toDoLists)
            {
                toDoList.ToDoItems = GetTodoItems(toDoList.Id).Where(r => r.DueDate <= DateTime.Now);
            }

            return Json(toDoLists, JsonRequestBehavior.AllowGet);
        }

        // GET: ToDoList/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ToDoList toDoList = db.ToDoLists.Find(id);
            if (toDoList == null)
            {
                return HttpNotFound();
            }
            return View(toDoList);
        }

        // GET: ToDoList/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ToDoList/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create([Bind(Include = "Id,Title,Description")] ToDoList toDoList)
        {
            if (ModelState.IsValid)
            {
                toDoList.CreateDate = DateTime.Now;
                toDoList.IsActive = true;
                db.ToDoLists.Add(toDoList);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(toDoList);
        }

        // GET: ToDoList/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ToDoList toDoList = db.ToDoLists.Find(id);
            if (toDoList == null)
            {
                return HttpNotFound();
            }
            return View(toDoList);
        }

        // POST: ToDoList/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit([Bind(Include = "Id,Title,Description,CreateDate,IsActive")] ToDoList toDoList)
        {
            if (ModelState.IsValid)
            {
                db.Entry(toDoList).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(toDoList);
        }

        // POST: ToDoList/Delete/5
        [HttpPost]
        public JsonResult DeleteConfirmed(string id)
        {
            var _id = int.Parse(id);
            ToDoList toDoList = db.ToDoLists.Find(_id);
            db.ToDoLists.Remove(toDoList);
            db.SaveChanges();
            return Json(new { redirectToUrl = Url.Action("Index") });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
