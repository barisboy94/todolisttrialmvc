﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ToDoListTrial.Context;
using ToDoListTrial.Models;
using ToDoListTrial.ViewModels;

namespace ToDoListTrial.Controllers
{
    public class ToDoItemController : Controller
    {
        private ToDoContext db = new ToDoContext();

        // GET: ToDoItem
        public ActionResult Index(int? toDoListId)
        {
            var model = new ToDoListViewModel();
            if (toDoListId != null)
            {
                ToDoList toDoList = db.ToDoLists.Find(toDoListId);
                if (toDoList == null)
                {
                    return HttpNotFound();
                }
                model.ToDoListId = toDoList.Id;
                model.ToDoListTitle = toDoList.Title;
                model.ToDoItems = GetTodoItems(toDoList.Id);
            }
            else
            {
                model.Message = "Yapılacaklar listesi seçmelisiniz.";
            }

            return View(model);
        }

        public IEnumerable<ToDoItem> GetTodoItems(int toDoListId)
        {
            var listItems = db.ToDoItems.ToList().Where(r => r.ToDoListId == toDoListId);

            return listItems;
        }

        // GET: ToDoItem/Create
        public ActionResult Create(int toDoListId)
        {
            var toDoItem = new ToDoItem()
            {
                ToDoListId = toDoListId,
                DueDate = DateTime.Now
            };
            return View(toDoItem);
        }

        // POST: ToDoItem/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create([Bind(Include = "Id,ToDoListId,Content,DueDate")] ToDoItem toDoItem)
        {
            if (ModelState.IsValid)
            {
                toDoItem.CreateDate = DateTime.Now;
                toDoItem.IsActive = true;
                db.ToDoItems.Add(toDoItem);
                db.SaveChanges();
                return RedirectToAction("Index", new { toDoItem.ToDoListId });
            }

            return View(toDoItem);
        }

        // GET: ToDoItem/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ToDoItem toDoItem = db.ToDoItems.Find(id);
            if (toDoItem == null)
            {
                return HttpNotFound();
            }
            return View(toDoItem);
        }

        // POST: ToDoItem/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit([Bind(Include = "Id,ToDoListId,Content,DueDate,CreateDate,IsActive")] ToDoItem toDoItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(toDoItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { toDoItem.ToDoListId });
            }
            return View(toDoItem);
        }

        // POST: ToDoItem/Delete/5
        [HttpPost]
        public JsonResult DeleteConfirmed(string id)
        {
            var _id = int.Parse(id);
            ToDoItem toDoItem = db.ToDoItems.Find(_id);
            db.ToDoItems.Remove(toDoItem);
            db.SaveChanges();
            return Json(new { redirectToUrl = Url.Action("Index","ToDoList", new { toDoItem.ToDoListId }) });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
