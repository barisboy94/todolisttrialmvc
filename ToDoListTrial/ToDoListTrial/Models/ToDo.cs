﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToDoListTrial.Models
{
    public class ToDoList
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsActive { get; set; }

        public IEnumerable<ToDoItem> ToDoItems { get; set; }

    }

    public class ToDoItem
    {
        public int Id { get; set; }
        public int ToDoListId { get; set; }
        [Required]
        public string Content { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DueDate { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsActive { get; set; }

    }
}